package com.itssolution;

import com.itssolution.Enum.Gender;
import com.itssolution.dao.DepartmentDao;
import com.itssolution.dao.StudentDao;
import com.itssolution.model.Department;
import com.itssolution.model.Student;
import com.itssolution.view.DepartmentView;
import com.itssolution.view.StudentView;

import java.util.Scanner;

import static java.lang.System.exit;
import static java.lang.System.in;

public class Main {

    public static void main(String[] args) {
	// write your code here
        DepartmentDao departmentDao = new DepartmentDao();
        StudentDao studentDao = new StudentDao();
        StudentView studentView = new StudentView();
        DepartmentView departmentView = new DepartmentView();
        Main main= new Main();
        Scanner scanner = new Scanner(System.in);
        int choice=100;
        while (true)
        {
            main.allMenu();
            System.out.print("Please Enter your choice? ...");
            choice=scanner.nextInt();
            switch (choice)
            {
                case 0:
                    //code for exit
                    exit(0);
                case 11:
                    //code for list department
                    departmentView.displayList(departmentDao.getAll());
                    break;
                case 12:
                    //code for add department
                    departmentDao.insert(departmentView.createDepartment());
                    break;
                case 13:
                    // code for update department
                    System.out.print("Enter SN of Department which you want to update?..");
                    Department department =departmentDao.getAll().get(scanner.nextInt()-1);
                    Department updatedDepartment = departmentView.updateDepartment(department);
                    departmentDao.update(updatedDepartment);
                    break;
                case 14:
                    // code for delete department
                    int sn =departmentView.deleteDepartment();
                    departmentDao.delete(departmentDao.getAll().get(sn-1).getDepartmentId());
                    break;
                case 21:
                    //code to list student
                    studentView.displayList(studentDao.getAll());
                    break;
                case 22:
                    // code to add student
                    studentDao.insert(studentView.createStudent(departmentDao.getAll()));
                    break;
                case 23:
                    //code to update Student
                    System.out.println("Enter SN of Student which you want to update??..");
                    Student student= studentDao.getAll().get(scanner.nextInt()-1);
                    Student updatedStudent = studentView.updateStudent(departmentDao.getAll(),student);
                    studentDao.update(updatedStudent);
                    break;
                case 24:
                    //code to delete student
                    int index = studentView.delete()-1;
                    studentDao.delete(studentDao.getAll().get(index).getStudentId());
                    break;
                default:
                    // code for invalid choice
                    System.out.println("Invalid Input...Please Enter Valid Input ..");
                    break;
            }

        }



    }
    public void allMenu()
    {
        System.out.println();
        System.out.printf("%-35s","----1. Department Menu ----");
        System.out.printf("%-35s","----2. Student Menu -----");
        System.out.printf("%-35s","---- Exit Menu -----");
        System.out.println();
        System.out.printf("%-35s","11. List Department");
        System.out.printf("%-35s","21. List Student");
        System.out.printf("%-35s", "0. Exit");
        System.out.println();
        System.out.printf("%-35s","12. Add Department");
        System.out.printf("%-35s","22. Add Student");
        System.out.println();
        System.out.printf("%-35s","13. Update Department");
        System.out.printf("%-35s","23. Update Student");
        System.out.println();
        System.out.printf("%-35s","14. Delete Department");
        System.out.printf("%-35s","24. Delete Student\n");
        System.out.println();
    }


    
}
