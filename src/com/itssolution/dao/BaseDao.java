package com.itssolution.dao;

import java.util.List;

public interface BaseDao<T> {
    boolean insert(T t);
    boolean update(T t);
    List<T> getAll();
    T getById(int id);
    boolean delete(int id);
}
