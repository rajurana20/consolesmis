package com.itssolution.Enum;

public enum Gender {
    MALE,
    FEMALE;
    public static Gender getGender(int ordinal)
    {
        for (Gender gender : Gender.values())
        {
            if (gender.ordinal() == ordinal)
                return gender;
        }
        return null;
    }
}
