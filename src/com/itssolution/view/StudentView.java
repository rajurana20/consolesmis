package com.itssolution.view;

import com.itssolution.Enum.Gender;
import com.itssolution.model.Department;
import com.itssolution.model.Student;
import com.itssolution.util.DateConverter;

import java.time.LocalDate;
import java.util.List;
import java.util.Scanner;

public class StudentView {
    Scanner viewScanner= new Scanner(System.in);
    public void displayList(List<Student> studentList)
    {
        System.out.println("------------List of Students----------");
        System.out.printf("%-5s","SN");
        System.out.printf("%-20s","name");
        System.out.printf("%-20s","address");
        System.out.printf("%-20s","Gender");
        System.out.printf("%-20s","email");
        System.out.printf("%-20s","phoneNo");
        System.out.printf("%-20s","date-of-birth");
        System.out.printf("%-20s","department");
        System.out.println();
        for (Student stu:studentList)
        {
            System.out.printf("%-5s",studentList.indexOf(stu)+1);
            System.out.printf("%-20s",stu.getName());
            System.out.printf("%-20s",stu.getAddress());
            System.out.printf("%-20s", stu.getGender());
            System.out.printf("%-20s",stu.getEmail());
            System.out.printf("%-20s",stu.getPhoneNo());
            System.out.printf("%-20s",stu.getDob());
            System.out.printf("%-20s",stu.getDepartment().getDepartmentName());
            System.out.println();
        }
    }

    public Student createStudent(List<Department> departmentList)
    {
        Student student = new Student();
        System.out.println("---------- Create New Student ---------");
        System.out.print("Enter Name : ? .. "); student.setName(viewScanner.nextLine());
        System.out.print("Enter Address : ? .."); student.setAddress(viewScanner.nextLine());
        System.out.print("Enter Email : ? .."); student.setEmail(viewScanner.nextLine());
        System.out.print("Enter Phone No : ? .."); student.setPhoneNo(viewScanner.nextLine());
        System.out.print("Enter Date of Birth (formate : 1994-11-28) : ? ..");
        student.setDob(DateConverter.toLocalDate(viewScanner.nextLine()));

        System.out.println("---- Gender -----");
        for (Gender gen:Gender.values())
        {
            System.out.println("\t" + (gen.ordinal()+1) +". " + gen );
        }
        System.out.print("Enter Gender : ? ... \n ");
        student.setGender(Gender.getGender(viewScanner.nextInt()-1));

        System.out.printf("%-5s","SN");
        System.out.printf("%-20s","Name");
        System.out.println();
        for (Department department:departmentList)
        {
            System.out.printf("%-5s",departmentList.indexOf(department)+1);
            System.out.printf("%-20s",department.getDepartmentName());
            System.out.println();
        }
        System.out.println("Enter Department SN ");
        student.setDepartment(departmentList.get(viewScanner.nextInt()-1));
        System.out.print("");
        return student;
    }

    public Student updateStudent(List<Department> departmentList, Student student)
    {
        System.out.println("---------- Update Student ---------");
        System.out.println("Note: If you want to change value of field then type new value or else just hit enter for no change");
        System.out.print("Enter Name : " + student.getName()+" ??..");
        String name = viewScanner.nextLine();
        if (!name.equals(""))
            student.setName(name);

        System.out.print("Enter Address : " + student.getAddress()+" ??..");
        String address = viewScanner.nextLine();
        if (!address.equals(""))
            student.setAddress(address);

        System.out.print("Enter Email : "+ student.getEmail()+" ?? ..");
        String email = viewScanner.nextLine();
        if (!email.equals(""))
            student.setEmail(email);

        System.out.print("Enter Phone No : "+ student.getPhoneNo()+" ?? ..");
        String phoneNo =viewScanner.nextLine();
        if(!phoneNo.equals(""))
            student.setPhoneNo(phoneNo);

        System.out.print("Enter Date of Birth (formate : 1994-11-28) : "+ student.getDob() + " ?? ..");
        String strDob = viewScanner.nextLine();
        if (!strDob.equals(""))
            student.setDob(DateConverter.toLocalDate(strDob) );

        System.out.println("----Gender ----");
        for (Gender gen:Gender.values())
        {
            System.out.println("\t" + (gen.ordinal()+1) +". " + gen );
        }
        System.out.print("Enter Gender : " + (student.getGender().ordinal()+1)+ ". "+student.getGender() + " ?? ..");
        String strGen = viewScanner.nextLine();
        if (!strGen.equals(""))
            student.setGender(Gender.getGender(Integer.parseInt(strGen)-1));

        System.out.println("--------Department-----");
        System.out.printf("%-5s","SN");
        System.out.printf("%-20s","Name");
        System.out.println();
        for (Department department:departmentList)
        {
            System.out.printf("%-5s",departmentList.indexOf(department)+1);
            System.out.printf("%-20s",department.getDepartmentName());
            System.out.println();
        }
        System.out.println("Enter Department SN for the Student ");
        String strDeptId = viewScanner.nextLine();
        if (!strDeptId.equals(""))
            student.setDepartment(departmentList.get(Integer.parseInt(strDeptId)-1));
        System.out.print("");
        return student;
    }

    public int delete()
    {
        System.out.print("Enter a SN of Student which you want to delete??..");
        return viewScanner.nextInt();
    }

}



