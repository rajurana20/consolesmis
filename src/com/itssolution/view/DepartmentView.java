package com.itssolution.view;

import com.itssolution.model.Department;

import java.util.List;
import java.util.Scanner;

public class DepartmentView {
    Scanner viewScanner = new Scanner(System.in);
    public void displayList(List<Department> departmentList)
    {
        System.out.println("------------List of Departments----------");
        System.out.printf("%-5s","SN");
        System.out.printf("%-20s","Name");
        System.out.printf("%-20s","Description");
        System.out.println();
        for (Department department:departmentList)
        {
            System.out.printf("%-5s",departmentList.indexOf(department)+1);
            System.out.printf("%-20s",department.getDepartmentName());
            System.out.printf("%-20s",department.getDescription());
            System.out.println();
        }
    }
    public Department createDepartment()
    {
        Department department = new Department();
        System.out.println("---------- Create New Department ---------- ");
        System.out.printf("%-20s","Name??....");
        department.setDepartmentName(viewScanner.nextLine());
        System.out.printf("%-20s","Description??....");
        department.setDescription(viewScanner.nextLine());
        return department;
    }
    public Department updateDepartment(Department department)
    {
        System.out.println("------------Update Department Information----------");
        System.out.printf("%-20s","Name");
        System.out.printf("%-20s","Description");
        System.out.println();
        System.out.printf("%-20s",department.getDepartmentName());
        System.out.printf("%-20s",department.getDescription());
        System.out.println();
        System.out.println("Note: If you do not want to change any field just Press enter... ");
        System.out.println("If you do want to change the field then enter modified value... ");
        System.out.print("Enter Department Name: " + department.getDepartmentName()+" ?? .. ");
        String name =viewScanner.nextLine();
        if (!name.equals(""))
            department.setDepartmentName(name);
        System.out.print("Enter Description: " + department.getDescription()+" ?? .. ");
        String description =viewScanner.nextLine();
        if (!description.equals(""))
            department.setDescription(description);
        return department;
    }

    public int deleteDepartment()
    {
        System.out.print("Enter SN of Department that you want to delete?? ..");
        return viewScanner.nextInt();
    }
}
