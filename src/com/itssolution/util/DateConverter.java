package com.itssolution.util;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class DateConverter {
    public static LocalDate toLocalDate(String strDate)
    {
        LocalDate localDate = LocalDate.parse(strDate);
        return localDate;
    }
    public static String toString(LocalDate localDate)
    {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        String strDate = formatter.format(localDate);
        return strDate;
    }
}
