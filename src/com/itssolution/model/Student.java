package com.itssolution.model;

import com.itssolution.Enum.Gender;
import com.itssolution.dao.DepartmentDao;

import java.time.LocalDate;

public class Student {
    private int studentId;
    private String name;
    private String address;
    private Gender gender;
    private String email;
    private String phoneNo;
    private LocalDate dob;
    private Department department;

    public Student() {
    }

    public Student(String name, String address, Gender gender, String email, String phoneNo, LocalDate dob, int deptId) {
        this.name = name;
        this.address = address;
        this.gender = gender;
        this.email = email;
        this.phoneNo = phoneNo;
        this.dob = dob;
        this.department=(new DepartmentDao()).getById(deptId);
    }

    public int getStudentId() {
        return studentId;
    }

    public void setStudentId(int studentId) {
        this.studentId = studentId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public LocalDate getDob() {
        return dob;
    }

    public void setDob(LocalDate dob) {
        this.dob = dob;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    @Override
    public String toString() {
        return "Student{" +
                "studentId=" + studentId +
                ", name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", gender=" + gender +
                ", email='" + email + '\'' +
                ", phoneNo='" + phoneNo + '\'' +
                ", dob=" + dob +
                '}';
    }
}
